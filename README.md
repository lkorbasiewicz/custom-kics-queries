# custom-kics-queries

some custom kics queries for testing sast custom rulesets

## Getting started

```toml
[kics]
  description = 'kics custom rules configuration for SAST IaC'
  [[kics.passthrough]]
    type  = "git"
    value = "https://gitlab.com/theoretick/custom-kics-queries"
    # we are cloning the main branch
    ref = "refs/heads/main"
```
